#!/bin/bash

INSTANCE=$1
INSTANCE=$((${INSTANCE}-1))

PIDLIST=""
MEMHOG_PATH="./"

SLOW_NUMA_NODE_CPUS=`numactl -H| grep "node 0 cpus" | cut -d" " -f 4-`
read -a CPUS_ARRAY <<< "${FAST_NUMA_NODE_CPUS}"

for i in $(seq 0 ${INSTANCE})
do
	CPU=$((i*2))
	#echo ${CPU}
	taskset -c ${CPU} ${MEMHOG_PATH}/memhog -r1 50m membind 0  &>/dev/null &
	#./numactl -N 0 -m 0 ./stream_forever &>/dev/null &
	PIDLIST="$! ${PIDLIST}"
	echo -1000 | sudo tee /proc/$!/oom_score_adj
done

trap "kill ${PIDLIST}; exit" INT
echo "Running"

for pid in ${PIDLIST}
do
	wait ${pid}
done